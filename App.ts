/***************************************************************
 * Entry point of the App
 ***************************************************************/
/** import */
import App from './src/App';
/** export */
export default App;