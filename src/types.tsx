/***************************************************************
 * config types
 ***************************************************************/
/** import type */
import { NavigatorScreenParams } from '@react-navigation/native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

/***************************************************************
 * type route
 ***************************************************************/
/** config global type of ReactNavigation */
declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParamList {}
  }
}

/** root route */
export type RootStackParamList = {
  AfterLogin: NavigatorScreenParams<AfterLoginParamList> | undefined;
  BeforeLogin: NavigatorScreenParams<BeforeLoginParamList> | undefined;
}

/** after login route */
export type AfterLoginParamList = {
  // for top of afterLogin
  Home: undefined;
}
/** after login screens props navigation  */
export type AfterLoginScreenProps<Screen extends keyof AfterLoginParamList> = NativeStackScreenProps<AfterLoginParamList, Screen>;
/** before login route */
export type BeforeLoginParamList = {
  // for top of login
  SignIn: undefined;
  SignUp: undefined;
}
/** before login screens props navigation  */
export type BeforeLoginScreenProps<Screen extends keyof BeforeLoginParamList> = NativeStackScreenProps<BeforeLoginParamList, Screen>;
