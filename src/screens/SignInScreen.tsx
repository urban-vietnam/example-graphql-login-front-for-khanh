/** import react native */
import React, { useState } from 'react';
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native';
import { Input, Button } from 'react-native-elements';
import { FontAwesome } from '@expo/vector-icons';
/** import apollo */
import { useApolloClient, useMutation } from '@apollo/client';
/** import graphql */
import { GET_ME, LOGIN } from '../graphql/documents';
/** import util */
import { signIn, signOut } from '../storage/AuthenticationStorage';
/** import type */
import { BeforeLoginScreenProps } from '../types';
import { Login, LoginVariables } from '../graphql/documents';

/***************************************************************
 * render sign in screen
 ***************************************************************/
/**
 * render parts of Icon
 */
function FormICon(props: { name: React.ComponentProps<typeof FontAwesome>['name']; color: string; onPress?: () => void }) {
    return <FontAwesome size={20} style={{ padding: 10 }} {...props} />;
}

/**
 * render parts of Sign in form
 */
const SignInForm = () => {
    // define variables
    const [loginId, setLoginId] = useState<LoginVariables['email']>(''); // login id = email
    const [loginPw, setLoginPw] = useState<LoginVariables['password']>(''); // login password = password
    const [isShowPassword, setIsShowPassword] = useState(false); // is show  password
    const client = useApolloClient(); // use client App
    const [loginMutation] = useMutation<Login, LoginVariables>(LOGIN, { // use login mutation process
        onCompleted: async (data) => { // handle login mutation success
            if (!data) return; // if error pattern -> end.
            // logout here = modify new user 
            await client.clearStore(); // clear apollo client cache
            await signOut(); // clear storage
            // complete login success
            await signIn(data.login.token); // ← complete login token = set storage
        },
        // update user query cache
        update(cache, { data }) {
            // write query GET_ME = after login user
            if (data) cache.writeQuery({ query: GET_ME, data: {getCurrentUser: data.login} }); // ← this update query = refetch RootNavigator useQuery<GetMe>(GET_ME)
        },
    });

    /**
     * execute login mutation
     * @param {string} email login id
     * @param {string} password login pw
     */
    const login = async (email: string, password: string) => {
        // mutation login execute
        return loginMutation({variables: {email, password}});
    };

    /**
     * handle on Submit (login)
     */
    const handleOnSubmit = () => {
        // sample vallidate
        if (!loginId) alert('Oh! id');
        if (!loginPw) alert('Oh! pw');
        // TODO: and more validate...

        // validate OK = auth
        login(loginId, loginPw); // mutation login execute
    }

    // render SignIn Form
    return (
        <View style={styles.form}>
            {/* login id  */}
            <Input
                inputContainerStyle={styles.input}
                placeholder='id: xxx@yyy.jp'
                leftIcon={<FormICon name='mail-forward' color='#a8a8a8' />}
                onChangeText={(val = '') => setLoginId(val)}
            />
            {/* login pw  */}
            <Input
                inputContainerStyle={styles.input}
                placeholder='password'
                maxLength={16}
                secureTextEntry={isShowPassword ? false : true}
                leftIcon={<FormICon name='lock' color='#a8a8a8' />}
                rightIcon={
                    <FormICon name={isShowPassword ? 'eye' : 'eye-slash'} color='#a8a8a8' onPress={() => setIsShowPassword(!isShowPassword)}/>
                }
                onChangeText={(val = '') => setLoginPw(val)}
            />
            {/* submit login */}
            <Button
                buttonStyle={{ width: 150, alignSelf: 'center', borderRadius: 50, backgroundColor: '#fa466d', marginTop: 25 }}
                title='Sign in'
                onPress={() => handleOnSubmit()}
                disabled={!loginId || !loginPw}
            />
        </View>
    );
};

/** export sign in navigation */
export default function SignInScreen({ navigation }: BeforeLoginScreenProps<'SignIn'> ) {

    // render sign in screen
    return (
        // wrap = sage area view
        <SafeAreaView style={styles.container}>
            {/* header */}
            <View style={styles.title_wrapper}>
                <Text style={styles.title}>Login</Text>
            </View>
            {/* content */}
            <SignInForm />
            {/* footer */}
            <View style={styles.footer}>
                <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
                    <Text style={{ color: '#adaeaf' }}>Don't have an account ?</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
}

/** define design */
const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        height: "100%",
        width: "100%"
    },
    title_wrapper: {
        marginVertical: 30,
        alignSelf: 'center',
    },
    title: {
        color: '#fa466d',
        fontSize: 35,
    },
    btn_back: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
    },
    form: {
        padding: 8
    },
    input: {
        borderWidth: 1,
        borderRadius: 50,
        backgroundColor: 'white',
        borderColor: '#a8a8a8',
        marginTop: 15
    },
    footer: {
        flex: 1,
        flexDirection: 'row',
        padding: 5,
        marginTop: '10%',
        justifyContent: 'space-around'
    }
});