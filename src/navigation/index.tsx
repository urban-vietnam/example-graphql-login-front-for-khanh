/***************************************************************
 * root navigation this applicatation
 ***************************************************************/
/** import react native */
import React from 'react';
import { ColorSchemeName } from 'react-native';
/** import react navigation */
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import AfterLoginNavigator from './AfterLoginNavigator';
import BeforeLoginNavigator from './BeforeLoginNavigator';
/** import apollo client */
import { useQuery } from '@apollo/client';
/** import graphql */
import { GET_ME } from '../graphql/documents';
/** import screen */
import SplashScreen from '../screens/SplashScreen';
/** import type */
import { RootStackParamList } from '../types';
import { GetMe } from '../graphql/documents';

/***************************************************************
 * render root routes
 ***************************************************************/
// A root stack navigator
const Stack = createNativeStackNavigator<RootStackParamList>();

// render root navigation
const RootNavigator = () => {
  // 認証情報取得 (token を 端末から取得と query user実行)
  const { data, loading } = useQuery<GetMe>(GET_ME); // useQuery(user) = get current user request -> get user: has token && token is valid / else: token is not valid (cause expired or test token or ... something)

  // render = complete request query
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      {
        // ログイン状態別に 主な ナビゲーションを変更
        (data?.getCurrentUser && !loading) // has token && get user: token is valid
        ? (<Stack.Screen name='AfterLogin' component={AfterLoginNavigator} />) // ログイン済 => After
        : (<Stack.Screen name='BeforeLogin' component={BeforeLoginNavigator} />) // ログイン前 => Before
      }
    </Stack.Navigator>
  );
}

/** export root navigation */
export default function RootNavigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
    // 「Root navigator」 is used for screen transitions. NavigationContainer is wrapper.
    <NavigationContainer theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      <RootNavigator />
    </NavigationContainer>
  );
}
