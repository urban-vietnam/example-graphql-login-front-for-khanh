/***************************************************************
 * アプリ ログイン後のナビゲーション
 ***************************************************************/
/** import react native */
import * as React from 'react';
/** import react navigation */
import { createNativeStackNavigator } from '@react-navigation/native-stack';
/** import screens */
import HomeScreen from '../screens/HomeScreen';
/** import type */
import { AfterLoginParamList } from '../types';

/***************************************************************
 * アプリ ログイン後のナビゲーション
 ***************************************************************/
// スタックナビゲーション にて 定義
const AfterLogin = createNativeStackNavigator<AfterLoginParamList>();
/** export after login navigation */
export default function AfterLoginNavigator() {
  return (
    <AfterLogin.Navigator initialRouteName='Home'>
      <AfterLogin.Screen name='Home' component={HomeScreen} />
    </AfterLogin.Navigator>
  );
}
