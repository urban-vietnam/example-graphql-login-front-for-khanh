/***************************************************************
 * アプリ ログイン前のナビゲーション
 ***************************************************************/
/** import react native */
import * as React from 'react';
/** import react navigation */
import { createNativeStackNavigator } from '@react-navigation/native-stack';
/** import screens */
import SignInScreen from '../screens/SignInScreen';
import SignUpScreen from '../screens/SignUpScreen';
/** import type */
import { BeforeLoginParamList } from '../types';

/***************************************************************
 * アプリ ログイン前のナビゲーション
 ***************************************************************/
// スタックナビゲーション にて 定義
const BeforeLogin = createNativeStackNavigator<BeforeLoginParamList>();
/** export before login navigation */
export default function BeforeLoginNavigator() {
  return (
    <BeforeLogin.Navigator initialRouteName='SignIn'>
      <BeforeLogin.Screen name='SignIn' component={SignInScreen} />
      <BeforeLogin.Screen name='SignUp' component={SignUpScreen} />
    </BeforeLogin.Navigator>
  );
}