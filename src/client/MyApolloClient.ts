/***************************************************************
 * custom Apollo Client
 ***************************************************************/
/** import react */
import { useState, useEffect } from 'react';
/** import apollo */
import { ApolloClient, InMemoryCache, ApolloLink, createHttpLink, from } from '@apollo/client';
import { onError } from '@apollo/client/link/error';
/** import util */
import { getAuthToken, signOut } from '../storage/AuthenticationStorage';
/** import type */
import { NormalizedCacheObject } from '@apollo/client';

/***************************************************************
 * private constant
 ***************************************************************/
/** link URL */
const BASE_URL = 'http://localhost:8080';
// make link GraphQL Client
const httpLink = createHttpLink({uri: `${BASE_URL}/graphql`});
/** 利用する InMemoryCache初期化 */
const cache = new InMemoryCache();

/** customize Apollo Client. cause authnication HttpHeader manage */
const authMiddleware = (jwt: string | null) => new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  operation.setContext(({ headers = {} }) => {
    // return custom header
    return { headers: { ...headers, Authorization: jwt ? `Bearer ${jwt}` : '', } }; // has ? Bearer JWT : ''
  });
  // next operation
  return forward(operation);
})
/** customize Apollo Client. cause handle error to logout */
const handleOnErrorLink = onError(({ graphQLErrors, operation, forward }) => {
  // handle GraphQL Errors 
  if (graphQLErrors) {
    for (let err of graphQLErrors) {
      // handle error status code
      switch (err.extensions.code) {
        // TODO: Server sets code to UNAUTHENTICATED (401)
        // when an AuthenticationError is thrown in a resolver
        case 'UNAUTHENTICATED':
          // sigon out execute. cause UNAUTHENTICATED = is not me
          signOut();
          // modify the HttpHeader with a new token
          const oldHeaders = operation.getContext().headers;
          operation.setContext({ headers: { ...oldHeaders, authorization: '',}});
          // next operation
          return forward(operation);
      }
    }
  }
})

/***************************************************************
 * export
 ***************************************************************/
/**
 * ApolloClient生成
 * @returns {ApolloClient} 利用する ApolloClient
 */
export const useClient = (): ApolloClient<NormalizedCacheObject> | undefined => {
  // 結果
  const [client, setClient] = useState<ApolloClient<NormalizedCacheObject>>();

  // build Apollo client...
  useEffect(() => {
    // ApolloClient生成 
    const makeClient = async () => {
      // get JWT from local
      const jwt = await getAuthToken();
      // build Apollo Client
      const apolloClient = new ApolloClient({
        link: from([handleOnErrorLink, authMiddleware(jwt), httpLink]), // 認証ヘッダーを Http 通信クライアントに設定 httpLinkにて 外部のGraphQLへアクセス
        cache // データのキャッシュ
      });
      // update result
      setClient(apolloClient);
    }
    // ApolloClient生成
    makeClient();
  }, [setClient]);

  // 結果返却
  return client;
};

