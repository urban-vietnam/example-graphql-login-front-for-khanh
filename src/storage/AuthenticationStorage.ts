/***************************************************************
 * manage Storage utils
 ***************************************************************/
import AsyncStorage from '@react-native-async-storage/async-storage'; // <- for web
/***************************************************************
 * private constant
 ***************************************************************/
/** define key of storage  */
const AUTH_TOKEN_KEY = 'MY_JWT_TOKEN';
/***************************************************************
 * export util
 ***************************************************************/
// get from Store fuction
export const getAuthToken = (): Promise<string | null> => AsyncStorage.getItem(AUTH_TOKEN_KEY);
// for login
export const signIn = (newToken: string): Promise<void> => AsyncStorage.setItem(AUTH_TOKEN_KEY, newToken);
// for logout
export const signOut = (): Promise<void> => AsyncStorage.removeItem(AUTH_TOKEN_KEY); // remove from storage

