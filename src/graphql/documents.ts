/***************************************************************
 * GraphQL の 集約
 ***************************************************************/
/** import apollo */
import { gql } from '@apollo/client';
/***************************************************************
 * export define type(Model) for graphql schema
 ***************************************************************/
// for graphql type ID
type ID = string;
/** type profile */
export type Profile = {
  id: ID
  username?: string
  email?: string
  firstName?: string
  lastName?: string
  age?: string
  address?: string
  telephoneNumber?: string
};
/** type User */
export type User = {
  id: ID
  token: string
  roles?: [string]
  profile?: Profile
};

/***************************************************************
 * define Fragmentation
 ***************************************************************/
/** alias fragmentation */
const FRAGMENT_ME = gql`
  fragment ME on User {
    id
    profile{
      username
      age
    }
  }
`;
/***************************************************************
 * export define Query graphql schema
 ***************************************************************/
/** get after login me interface */
export interface GetMe {
  getCurrentUser: User
}
/** get current user query gql */
export const GET_ME = gql`
  ${FRAGMENT_ME} # use framentation = alias on user
  # task all query gql 定義
  query GetMe {
    getCurrentUser {
      ...ME
    }
  }
`;

/***************************************************************
 * export define Mutation graphql schema
 ***************************************************************/
/** login interface */
export interface Login {
  login: User
}
/** login interface variables */
export type LoginVariables = {
  email: string
  password: string
}
/** login mutation */
export const LOGIN = gql`
  ${FRAGMENT_ME} # use framentation = alias on user
  # login mutation gql 定義
  mutation login($email: String!, $password: String!) {
    login(email: $email password: $password){
      token
      ...ME
    }
  }
`;