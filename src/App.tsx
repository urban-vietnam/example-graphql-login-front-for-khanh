/***************************************************************
 * Entry point of the App
 ***************************************************************/
/** import react native */
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { StatusBar } from 'expo-status-bar';
/** import apollo */
import { ApolloProvider } from '@apollo/client';
import { useClient } from './client/MyApolloClient';
/** import hooks */
import useColorScheme from './hooks/useColorScheme';
/** import nabigation */
import RootNavigation from './navigation';

/***************************************************************
 * render
 ***************************************************************/
export default function App() {
  // static content load...
  const colorScheme = useColorScheme(); // load theme ...
  const client = useClient(); // build apollo client ...

  // build not complete Client
  if (!client) return null; // splash screen

  // complete build client = rennder
  return (
    // wrap apollo client
    <ApolloProvider client={client} >
      <SafeAreaProvider>
        {/* to root navigation */}
        <RootNavigation colorScheme={colorScheme} />
        {/* for SP status bar */}
        <StatusBar />
      </SafeAreaProvider>
    </ApolloProvider>
  )
}
